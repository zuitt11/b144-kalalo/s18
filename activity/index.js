console.log("Ash managed to catch the Team Rocket and will soon go into battling them.")

//TRAINERS//

let ashKetchum = {
	firstName: 'Ash',
	lastName: 'Ketchum',
	pokemon:{
		Pikachu: `Pikachu`,
		Bulbasaur: `Bulbasaur`,
		Syndaquil: `Syndaquil`
	},
	friends: ['Brock',
		'Misty'],
}

let teamRocket = {
	firstName: 'Team',
	lastName: 'Rocket',
	pokemon:{
		Arbok: `Arbok`,
		Lickitung: `Lickitung`,
		Meowth: `Meowth`
	},
	friends: ['Cassidy',
		'Giovanni', 'Butch'],
	talk: function() {
		console.log("♪ Jessie: Prepare for trouble!");
		console.log("James: Make it double!");
		console.log("Jessie: To protect the world from devastation!");
		console.log("James: To unite all peoples within our nation!");
		console.log("Jessie: To denounce the evils of truth and love!");
		console.log("James: To extend our reach to the stars above!");
		console.log("Jessie: Jessie!");
		console.log("James: James!");
		console.log("Jessie: Team Rocket blasts off at the speed of light!");
		console.log("James: Surrender now or prepare to fight!");
		console.log("Meowth: Meowth! That's right! ♪");
	}
}

//POKEMON//
function Pokemon(name, level, element){
	this.name = name;
	this.level = level;
	this.element = element;
	this.health = 2*level;
	this.attack = 1.5*level;
	// ATTACKS
	this.faint = function(target) {
		console.log (target.name + ' has fainted.')
	};
	this.tackle = function (target) {
		console.log(this.name + ` used tackle against ` + target.name);
		console.log(this.name + ` and ` + target.name + ` are both unable to move due to the damage of the move`)
		if (target.health <= 0) {
			console.log(target.name + ' has fainted.')
		};
	};
	this.surf = function(target) {
		console.log(this.name + ` used tackle against ` + target.name);
		console.log(this.name + ` and ` + target.name + ` both are soaked in water`);
	};
	this.thunderbolt = function(target) {
		console.log(this.name + ` used surf against ` + target.name)
	};
	this.teamRocketExit = function(target) {
		console.log ('Because of the impact of the explosion, Team Rocket flew up in the sky.');
		console.log (`${TeamRocket.name}: *blown up* We shall returrrrrnnnn!`);
	};
};


function trainer (name, age, location, health){
	this.name = name;
	this.age = age;
	this.location = location;
	this.health = health;
	//METHODS
	this.talk = function(target) {
		console.log (target.name + ' I choose you!')
	};
	this.faints = function(target) {
		console.log (this.name + ' has fainted.')
	};
};

let TeamRocket = new trainer('Team Rocket', '?', '?', 0);


let Meowth = new trainer('Meowth', '?', 'Team Rocket', 0);
console.log (Meowth)

let Jessie = new trainer ('Jessie', '?', 'Team Rocket', 0);
console.log (Jessie)

let James = new trainer('James', '?', 'Team Rocket', 0);
console.log (James)
console.log("You can't stop us from getting all these pokemons because...");


teamRocket.talk();



let Ash = new trainer('Ash', 99975394, 'Pallet Town', `?`);
console.log(Ash);
console.log(`${Ash.name}: I, Ash Kethum, will stop your evil plans!`);



let Pikachu = new Pokemon('Pikachu', 99, 'Electric');
Ash.talk(Pikachu);
console.log	(Pikachu);

let Arbok = new Pokemon('Arbok', '95', 'Poison');
James.talk(Arbok);
console.log (Arbok);

console.log(`A strong wind approches and a storm is brewing but the trainers are unfazed`);
console.log(`Pressured by the nearby Pokemon Police, ${James.name} decided to attack first, but...`);

Arbok.tackle(James);
console.log(Arbok.attack - James.health);


console.log (`${Jessie.name}: What an idiot, I will take care from here on out. Go Lickitung!`);

let Lickitung = new Pokemon ('Lickitung', '99', 'Normal');
console.log(Lickitung);

console.log(`Rain starts to fall`);
console.log(`Before ${Jessie.name} could command ${Lickitung.name}...`);

Lickitung.surf(Jessie);
console.log(Lickitung.attack - Jessie.health);

console.log(`Amused by the events, ${Ash.name} instructed ${Pikachu.name}.`);

console.log(`${Ash.name}: ${Pikachu.name} use thunderbolt!`);

Pikachu.thunderbolt(TeamRocket);
Pikachu.teamRocketExit();
TeamRocket.faints();

console.log(`${Ash.name} has once again save a number of Pokemon in the hands of Team Rocket`);