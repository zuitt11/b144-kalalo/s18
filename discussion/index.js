let grades = [98.5, 94.3, 89.2, 90.1];

console.log(grades[0])

//object- collection of related data and functionalities; usually represents real-world objects.

let grade = {
	//object initializer/literal notation - properties to describe the object. 'Key-value pair'
	math: 89.2,
	english: 98.5,
	science: 90.1,
	filipino: 94.3
}
console.log(grade)
//dot notation - used to access the specific property/value of the object; preferred in terms of object access
/*	
	let objectName = {
		keyA: valueA,
		keyB: valueB,
	}
	*/
console.log(grade.english)


let cellphone = {
	brand: 'Nokia3310',
	color: 'DarkBlue',
	mfd: 1999
}
console.log(typeof cellphone)

let student = {
	firstName: 'John',
	lastName: 'Smith',
	mobileNo: 09354126589,
	location: {
		city: 'Tokyo',
		country:'Japan'
	},
	email: ['john@mail.com', 'john.smith@mail.xyz'],

	fullName: (function name(fullName) {
		console.log (this.firstName + ' ' + this.lastName)
	})
}
student.fullName()
console.log(student.location.city);
console.log(student.email);
console.log(student.email[0]);
console.log(student.email[1]);


// array of objects

let contactList = [
{
	firstName:'John',
	lastName:'Smith',
	location:'Japan',
},
{
	firstName:'Jane',
	lastName:'Smith',
	location:'Japan',
},
{
	firstName:'Jasmine',
	lastName:'Smith',
	location:'Japan',
}
]
console.log(contactList[0].firstName)


let people = [
	{
		name: 'Juanita',
		age: 13
	},{
		name: 'Juanito',
		age: 13
	}
]

people.forEach (function(person){
	console.log(person.name)
})

// console.log(`${person[0].name} are in the list`)



////////////////////////////////////////////////////////////////////////////////////////
// CONSTRUCTOR FUNCTION
/*
-creates a reusable function to create several objects that have the same data structure.
-useful for creating copies/instaces of an object
*/

// object literals
/*
	let object {}
	instance distinct/unique objects
	let object = new object
	instance - concrete occurence of any object which emphasizes on the unique identity of it.
		function ObjectName(KeyA, KeyB){
			this.keyA = keyA.
			this.keyB = keyB.
		}
*/
// REUSEABLE FUNCTION/OBJECT METHOD
function Laptop (name, manufactureDate){
	this.name=name,
	this.manufactureDate = manufactureDate
}
// unique instance of the laptop function
let laptop1 = new Laptop(`Lenovo`, 2008)
console.log(`Result from creating objects using object constructors`)
console.log(laptop1)

// unique instance of the laptop function
let laptop2 = new Laptop(`MacBook`, 2020)
console.log(`Result from creating objects using object constructors`)
console.log(laptop2)


let computer = {}
let computer1 = new Object();
//accessing laptops
console.log(laptop1.name)
console.log(laptop1['name'])


let array = [laptop1, laptop2]
console.log(array[0].name)

///////////////////////////////////////////////////////////////////////////////////////
// initializing, adding, deleting, reassigning object properties

//initialized or added properties after the object was created.
	//useful for times when an object's properties are undetermined at the time of creating them.

let car = {}
// adding object property/ies

car.name = 'Honda Civic'
console.log(car)

car['manufactureDate'] = 2019
console.log(car)

// reassigning object properties - same property, different value
car.name = 'Honda'
console.log(car)

// deleting object properties

delete car['manufactureDate']
console.log(car)



//OBJECT METHODS
	//is a function which is a property of an object; also functions but they are related to a specific object.


let person = {
	name: 'John',
	talk: function (){
		console.log ('Hello! My name is '+ this.name)
	}
}
console.log(person)
person.talk()

//adding properties
person.walk = function(){
	console.log(this.name + ` walked 25 steps forward.`)
}
person.walk()

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address:{
		city: `Austin`,
		state: `Texas`
	},
	emails: ['joe@mail.com',
		'joesmith@mail.xyz'],
	introduce: function(){
		console.log('Hello! My name is '+ this.firstName +' '+ this.lastName)
	}
}
friend.introduce()

let pokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This pokemon tackled tarket Pokemon')
		console.log("'targetPokemon's health is now reduced to _targetPokemonHealth")
	}
}

function Pokemon(name, level) {
	this.name = name;
	this.level = level
	this.health = 2*level
	this.attack = level;

//METHOD
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name)
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	};
	this.faint = function(){
		console.log(this.name+' fainted.')
	}
};
let Pikachu = new Pokemon('Pikachu', 16);
let Charizard = new Pokemon('Charizard', 8);

Pikachu.tackle(Charizard)